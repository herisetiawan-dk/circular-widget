import 'dart:async';

import 'package:circular_trial/constants.dart';
import 'package:circular_trial/controller.dart';
import 'package:flutter/material.dart';

class CircularAnimation<T extends Widget> extends StatefulWidget {
  final Function(Widget) builder;
  final List<T> children;
  final CircularController controller;
  final int childOnSide;
  final Alignment alignment;
  final double childSpacing;
  final double childRadius;
  final double sensitivity;
  final Function(List<int>) needShow;
  CircularAnimation({
    @required this.builder,
    @required this.children,
    this.controller,
    this.needShow,
    this.childOnSide = CircularConstants.childOnSide,
    this.alignment = CircularConstants.alignment,
    this.childSpacing = CircularConstants.childSpacing,
    this.childRadius = CircularConstants.childRadius,
    this.sensitivity = CircularConstants.sensitivity,
  });
  @override
  _CircularAnimationState<T> createState() => _CircularAnimationState<T>();
}

class _CircularAnimationState<T extends Widget>
    extends State<CircularAnimation<T>> with SingleTickerProviderStateMixin {
  List<double> angles = [];
  AnimationController _animationController;
  Duration _animationDuration = Duration(milliseconds: 500);
  double dynamicAnimationValue = 0;
  double moveDistance = 0;
  double spacing;
  List<int> needShow = [];
  int manyToDelete = 0;
  @override
  void initState() {
    spacing = widget.childSpacing;
    List<int> childrenToKey = List.from(widget.children.asMap().keys.toList());
    final List<int> firstChild =
        childrenToKey.sublist(0, widget.childOnSide + 1).toList();
    final List<int> lastChild = childrenToKey
        .sublist(
            widget.children.length - widget.childOnSide, widget.children.length)
        .toList();
    needShow = [...lastChild, ...firstChild];
    for (int i = 0; i < needShow.length; i++) {
      angles.add((i * -spacing) + (widget.childOnSide * spacing));
    }

    _animationController =
        AnimationController(vsync: this, duration: _animationDuration)
          ..addListener(_animationListener);
    super.initState();
    widget.controller?.addListener(circularControllerListener);
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.needShow != null) {
      Timer(
        Duration.zero,
        () => widget.needShow(needShow),
      );
    }
    return widget.builder(
      Stack(
        children: <Widget>[
          Align(
            alignment: widget.alignment,
                      child: Transform.scale(
              scale: (widget.childRadius / MediaQuery.of(context).size.width) + MediaQuery.of(context).size.width * 0.0001,
              child: Container(
                width: widget.childRadius,
                height: widget.childRadius,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey.withOpacity(0.25),
                ),
              ),
            ),
          ),
          ...needShow
              .asMap()
              .keys
              .map((i) => childWrapper(i, context: context))
              .map(gestureWrapper)
              .toList(),
        ],
      ),
    );
  }

  void circularControllerListener() {
    if (widget.controller.manual) {
      moveTo(widget.controller.index);
    }
  }

  void _addWidget() {
    if (angles[widget.childOnSide] <= -spacing &&
        angles[widget.childOnSide] != 0.0) {
      int newIndex = needShow[0] - 1;

      if (newIndex < 0) {
        newIndex = widget.children.length - 1;
      }
      setState(() {
        needShow.insert(0, newIndex);
        needShow.removeLast();
        angles = [];
        for (int i = 0; i < needShow.length; i++) {
          angles.add((i * -spacing) + (widget.childOnSide * spacing));
        }
      });
    }
    if (angles[widget.childOnSide] > spacing &&
        angles[widget.childOnSide] != 0.0) {
      int newIndex = needShow.last + 1;
      if (newIndex >= widget.children.length) {
        newIndex = 0;
      }
      setState(() {
        needShow.removeAt(0);
        needShow.add(newIndex);
        angles = [];
        for (int i = 0; i < needShow.length; i++) {
          angles.add((i * -spacing) + (widget.childOnSide * spacing));
        }
      });
    }
  }

  void moveTo(int index) {
    int from = needShow.indexOf(index);
    int childLength = widget.children.length;
    if (from >= 0) {
      if (from < widget.childOnSide) {
        int newIndex = needShow.first - 1;
        setState(() {
          manyToDelete = widget.childOnSide - from;
          for (int i = manyToDelete; i > 0; i--) {
            if (newIndex < 0) {
              newIndex = childLength - 1;
            }
            needShow.insert(0, newIndex);
            angles.insert(0, angles.first + spacing);
            newIndex--;
          }
          _animationDuration = Duration(
              milliseconds: (500 * ((spacing / 2) / (from + 1))).toInt());
          moveDistance = -angles[needShow.indexOf(index)];
        });
        _animationController.forward();
      } else if (from >= widget.childOnSide) {
        int newIndex = needShow.last + 1;
        setState(() {
          manyToDelete = from - widget.childOnSide;
          for (int i = manyToDelete; i > 0; i--) {
            if (newIndex >= childLength) {
              newIndex = 0;
            }
            needShow.add(newIndex);
            angles.add(angles.last - spacing);
            newIndex++;
          }
          _animationDuration = Duration(
              milliseconds: (500 * ((spacing / 2) / (from + 1))).toInt());
          moveDistance = -angles[needShow.indexOf(index)];
        });
        _animationController.forward();
      }
    }
  }

  void _animationListener() {
    setState(() {
      dynamicAnimationValue = _animationController.value;
    });
    if (_animationController.value == 1) {
      setState(() {
        for (int i = 0; i < angles.length; i++) {
          angles[i] += moveDistance;
        }
        if (widget.controller.manual) {
          final index =
              needShow.indexOf(widget.controller.index) - manyToDelete;
          if (index >= 0) {
            if (index < widget.childOnSide) {
              for (int i = manyToDelete; i > 0; i--) {
                needShow.removeLast();
                angles.removeLast();
              }
            } else if (index >= widget.childOnSide) {
              for (int i = manyToDelete; i > 0; i--) {
                needShow.removeAt(0);
                angles.removeAt(0);
              }
            }
          }
        }
        moveDistance = 0;
        _animationController.value = 0;
      });
    }
  }

  Widget childWrapper(
    int index, {
    BuildContext context,
  }) {
    final angle = ((_animationController.value * moveDistance) + angles[index]);
    return Align(
      alignment: widget.alignment,
      child: Transform.rotate(
        angle: angle,
        child: Container(
          height: widget.childRadius,
          child: Column(
            children: <Widget>[
              Spacer(),
              Transform.rotate(
                angle: -angle,
                child: widget.children[needShow[index]],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget gestureWrapper(Widget child) {
    return GestureDetector(
      onPanUpdate: onPanUpdate,
      onPanEnd: onPanEnd,
      child: child,
    );
  }

  void onPanUpdate(DragUpdateDetails details) {
    final axisX = details.delta.dx;
    setState(() {
      for (int i = 0; i < angles.length; i++) {
        angles[i] -= axisX * 0.005;
      }
    });
    widget.controller?.indexAuto = needShow[widget.childOnSide];
    _addWidget();
  }

  void onPanEnd(DragEndDetails details) {
    final halfRadius = spacing / 2;
    int centered;
    int i = 0;
    while (i < angles.length && centered == null) {
      if (angles[i] < halfRadius && angles[i] > -halfRadius) {
        centered = i;
      }
      i++;
    }
    if (centered == null) {
      centered = widget.controller.index;
    }

    widget.controller?.indexAuto = needShow[centered];
    setState(() {
      _animationDuration = Duration(
          milliseconds: (500 * ((spacing / 2) / (centered + 1))).toInt());
      moveDistance = -angles[centered];
    });
    _animationController.forward();
  }
}
