import 'package:circular_trial/value.dart';
import 'package:flutter/material.dart';

class CircularController extends ValueNotifier<CircularValue> {
  CircularController({
    int index,
    bool manual,
  }) : super(CircularValue(
          index: index ?? 0,
          manual: manual ?? true,
        ));

        
  int get index => value.index;

  bool get manual => value.manual;

  set index(int index) {
    value = value.copyWith(
      index: index,
      manual: true,
    );
  }

  set indexAuto(int index) {
    value = value.copyWith(
      index: index,
      manual: false,
    );
  }
}
