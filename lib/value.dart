class CircularValue {
  final int index;
  final bool manual;
  CircularValue({
    this.index,
    this.manual,
  });

  CircularValue copyWith({
    int index,
    bool manual,
  }) {
    return CircularValue(
      index: index ?? this.index,
      manual: manual ?? this.manual,
    );
  }
}
