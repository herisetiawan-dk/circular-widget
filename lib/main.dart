import 'package:circular_trial/circular.dart';
import 'package:circular_trial/controller.dart';
import 'package:circular_trial/constants.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Circular Trial',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Circular Animation'),
        ),
        body: Container(
          child: BodyBuilder(),
        ),
        resizeToAvoidBottomPadding: false,
      ),
    );
  }
}

class BodyBuilder extends StatefulWidget {
  @override
  _BodyBuilderState createState() => _BodyBuilderState();
}

class _BodyBuilderState extends State<BodyBuilder> {
  CircularController circularController;
  List<int> coloredButton = [];

  @override
  void initState() {
    circularController = CircularController()..addListener(circularListener);
    super.initState();
  }

  int currentIndex = 0;
  List<int> needShow = [];
  int childOnSide = 6;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Center(
          child: IndexedStack(
            index: currentIndex,
            children: CircularConstants.children.map(_bgWrapper).toList(),
          ),
        ),

        Align(
                  child: CircularAnimation(
            childOnSide: childOnSide,
            childRadius: 550,
            // alignment: Alignment(0, 0),
            controller: circularController,
            builder: (widget) {
              return widget;
            },
            needShow: (show) {
              setState(() {
                needShow = show;
              });
            },
            children: CircularConstants.children
                .map((c) => _iconWrapper(c, context))
                .toList(),
          ),
        ),

        // Container(
        //   child: TextField(
        //     keyboardType: TextInputType.number,
        //     onSubmitted: (value) {
        //       circularController.index = int.parse(value);
        //     },
        //   ),
        // ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
                child: Text('To First'),
                onTap: () {
                  debugPrint(needShow.length.toString());
                  if (needShow.length - 1 <= childOnSide * 2)
                    circularController.index = needShow.first;
                }),
            InkWell(
                child: Text('To Last'),
                onTap: () {
                  if (needShow.length - 1 <= childOnSide * 2)
                    circularController.index = needShow.last;
                }),
          ],
        ),
      ],
    );
  }

  Widget _iconWrapper(IconData child, BuildContext context) {
    final int index = CircularConstants.children.indexOf(child);
    final bool selected = coloredButton.contains(index);
    return Container(
      width: 50,
      height: 50,
      key: Key('$index'),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: selected ? Colors.orange : Colors.white,
        border: Border.all(color: Colors.orange, width: 2),
      ),
      child: InkWell(
        onTap: () {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(
                  'Index ${CircularConstants.children.indexOf(child)} Pressed'),
            ),
          );
          setState(() {
            if (selected) {
              coloredButton.removeWhere((i) => i == index);
            } else {
              coloredButton.add(index);
            }
          });
        },
        child: Icon(
          child,
          color: selected ? Colors.white : Colors.orange,
        ),
      ),
    );
  }

  Widget _bgWrapper(IconData child) {
    return Container(
      child: Opacity(
        opacity: CircularConstants.childrenBgOpacity,
        child: Icon(
          child,
          size: CircularConstants.childrenBgSize,
          color: CircularConstants.childrenBgColor,
        ),
      ),
    );
  }

  void circularListener() {
    setState(() {
      currentIndex = circularController.index;
    });
  }
}
