import 'package:flutter/material.dart';

class CircularConstants {
  static const double childrenBgSize = 500;
  static const Color childrenBgColor = Colors.lightBlue;
  static const double childrenBgOpacity = 0.5;
  static const int childOnSide = 4;
  static const Alignment alignment = Alignment(0, -5);
  static const double childSpacing = 125 / 360;
  static const double childRadius = 300;
  static const double sensitivity = 0.005;
  static const List<IconData> children = [
    Icons.looks_one,
    Icons.looks_two,
    Icons.looks_3,
    Icons.looks_4,
    Icons.looks_5,
    Icons.looks_6,
    Icons.mail,
    Icons.free_breakfast,
    Icons.supervised_user_circle,
    Icons.ac_unit,
    Icons.accessibility,
    Icons.accessibility_new,
    Icons.adb,
    Icons.zoom_in,
    Icons.work,
    Icons.widgets,
    Icons.web
  ];
}
